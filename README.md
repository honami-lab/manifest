# Pixel Experience #

### Sync ###

```bash
# Initialize local repository
repo init -u https://gitlab.com/honami-lab/manifest -b thirteen
```

```bash
# Lazy
repo init --depth=1 -u https://gitlab.com/honami-lab/manifest -b thirteen
```

```bash
# Device manifest (alioth)
git clone https://gitlab.com/honami-lab/local_manifest.git -b alioth .repo/local_manifests
```

```bash
# Sync
repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```

### Build ###

```bash
# Set up environment
$ . build/envsetup.sh

# Choose a target
$ lunch aosp_$device-userdebug

# Build the code
$ mka bacon -jX
```
